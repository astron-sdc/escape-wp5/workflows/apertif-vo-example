# Explore the ASTRON VO in a Jupyter environment

This repository is adapted from https://github.com/zhengmeyer/first-binder.
It is intended to serve as an example for integration with ESAP, the ESCAPE ESFRI Science Analysis Platform.
